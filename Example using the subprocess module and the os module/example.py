import subprocess
import os

# Define the input and output file paths
input_file = 'input.txt'
output_file = 'output.txt'

# Define the pipeline steps as a list of shell commands
pipeline = [
    f"cat {input_file}", # read the input file
    "tr '[:lower:]' '[:upper:]'", # convert all text to uppercase
    "grep 'HELLO'", # filter lines that contain "HELLO"
    f"sort -r > {output_file}", # sort the remaining lines in reverse order and save to output file
]

# Execute the pipeline using subprocess
for step in pipeline:
    subprocess.run(step, shell=True)

# Check if the output file was created successfully
if os.path.exists(output_file):
    print(f"Output file '{output_file}' was created successfully.")
else:
    print(f"Failed to create output file '{output_file}'.")
