import os
import pandas as pd
import multiprocessing as mp
from typing import List

# Define the input and output directories
input_dir = 'input/'
output_dir = 'output/'

# Define a function to process a single file
def process_file(file_path: str) -> pd.DataFrame:
    # Read the input file into a pandas DataFrame
    df = pd.read_csv(file_path)
    
    # Apply some transformations to the data
    df['date'] = pd.to_datetime(df['date'])
    df['year'] = df['date'].dt.year
    df['month'] = df['date'].dt.month
    
    # Filter the data based on some conditions
    df = df[(df['value'] > 0) & (df['year'] >= 2021)]
    
    return df

# Define a function to process a batch of files in parallel
def process_files_batch(file_paths: List[str]) -> None:
    # Create a pool of worker processes
    with mp.Pool() as pool:
        # Process each file in parallel
        results = pool.map(process_file, file_paths)
    
    # Concatenate the results into a single DataFrame
    result_df = pd.concat(results)
    
    # Write the result to an output file
    output_file_path = os.path.join(output_dir, 'output.csv')
    result_df.to_csv(output_file_path, index=False)
    
    print(f"Processed {len(file_paths)} files and saved the result to '{output_file_path}'.")

# Get a list of all input files
input_files = [os.path.join(input_dir, f) for f in os.listdir(input_dir) if os.path.isfile(os.path.join(input_dir, f))]

# Split the input files into batches of 10
file_batches = [input_files[i:i+10] for i in range(0, len(input_files), 10)]

# Process each batch of files in parallel
for i, batch in enumerate(file_batches):
    process_files_batch(batch)

print("Done!")
